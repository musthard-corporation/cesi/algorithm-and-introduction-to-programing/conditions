import java.util.Scanner;

public class Example {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number : ");
        int number = scanner.nextInt();

        if (number <= 0 || number > 10) {
            System.out.printf("Number '%d' is not between 1 and 10%n", number);
            System.exit(1);
        }

        if (number % 2 == 0) {
            if (number == 2) {
                System.out.printf("Number '%d' is prime", number);
            } else {
                System.out.printf("Number '%d' is not prime", number);
            }
        } else if (number == 9) {
            System.out.printf("Number '%d' is not prime", number);
        } else {
            System.out.printf("Number '%d' is prime", number);
        }
    }

    private void simplified() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number : ");
        int number = scanner.nextInt();

        if (number <= 0 || number > 10) {
            System.out.printf("Number '%d' is not between 1 and 10", number);
            System.exit(1);
        }

        if ((number % 2 == 0 && number != 2) || number == 9) {
            System.out.printf("Number '%d' is not prime", number);
        } else {
            System.out.printf("Number '%d' is prime", number);
        }
    }
}