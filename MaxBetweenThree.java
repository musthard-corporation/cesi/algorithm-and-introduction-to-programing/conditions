import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class MaxBetweenThree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number : ");
        int a = scanner.nextInt();
        System.out.print("Enter second number : ");
        int b = scanner.nextInt();
        System.out.print("Enter third number : ");
        int c = scanner.nextInt();

        // maxBetweenThree(a, b, c);
        // refactoredSpecific(a, b, c);
        refactored(a, b, c);
    }

    static void maxBetweenThree(int a, int b, int c) {
        if (a == b && b == c) {
            System.out.printf("All values are equal to : %d", a);
        } else {
            int max = c;
            if (a > max) {
                max = a;
            }
            if (b > max) {
                max = b;
            }

            System.out.printf("Maximum given value is : %d", max);
        }
    }

    static void refactoredSpecific(int a, int b, int c) {
        System.out.println(Math.max(a, Math.max(b, c)));
    }

    static void refactored(Integer... values) {
        System.out.println(Collections.max(Arrays.asList(values)));
    }
}
