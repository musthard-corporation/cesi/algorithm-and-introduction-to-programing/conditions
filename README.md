# Base de la programmation et algorithmie - Structures conditionnelles

## Compilation d'un fichier java

```shell
javac ./<NomFichier>.java
```

## Exécution d'un programme java

```shell
java <NomFichier>
```

(Attention, ne pas indiquer de chemin ou d'extension)
