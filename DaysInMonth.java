import java.util.Scanner;

public class DaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter month number : ");
        int month = scanner.nextInt();

        if (month == 2) {
            System.out.printf("There a 28 days in month %d", month);
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            System.out.printf("There a 30 days in month %d", month);
        } else {
            System.out.printf("There a 31 days in month %d", month);
        }
    }
}
