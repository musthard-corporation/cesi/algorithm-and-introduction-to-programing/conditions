import java.util.Scanner;

public class CompareTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number : ");
        int a = scanner.nextInt();
        System.out.print("Enter second number : ");
        int b = scanner.nextInt();

        // compareTwo(a, b);
       refactored(a, b);
    }

    static void compareTwo(int a, int b) {
        if (a > b) {
            System.out.println(1);
        } else if (b > a) {
            System.out.println(-1);
        } else {
            System.out.println(0);
        }
    }

    static void refactored(int a, int b) {
        // (a < b) ? -1 : ((a == b) ? 0 : 1);
        System.out.println(Integer.compare(a, b));
    }
}
